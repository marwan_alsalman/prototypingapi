﻿using UnityEngine;

namespace PrototypingAPI
{
    public class Prototyping
    {
        public static Vector2 AngularPosition(Vector2 orign, float length, float angle)
        {
            Vector2 result;
            angle = angle * Mathf.Deg2Rad;
            result = orign + new Vector2(
                LengthDirectionX(length, angle),
                LengthDirectionY(length, angle));
            Debug.DrawLine(orign, result);
            return result;
        }

        public static RaycastHit2D PointCollision(Vector2 position, LayerMask collisionLayer)
        {
            RaycastHit2D hit = Physics2D.Raycast(position, Vector2.zero, 0.0f, collisionLayer);
            return hit;
        }

        public static float LengthDirectionX(float length, float angle)
        {
            return Mathf.Cos(angle) * length;
        }

        public static float LengthDirectionY(float length, float angle)
        {
            return Mathf.Sin(angle) * length;
        }

        public static Vector2 DirectionalVector(Vector2 from, Vector2 to)
        {
            return (to - from).normalized;
        }

        public static float DirectionToAngle(Vector2 direction)
        {
            return Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        }

        public static Vector2 AngleToDirection(float angle)
        {
            return new Vector2(Mathf.Cos(Mathf.Deg2Rad * angle),
                Mathf.Sin(Mathf.Deg2Rad * angle));
        }

        public static bool InLineOfSight(
            Vector2 from,
            Vector2 to,
            float range,
            LayerMask blockLayer,
            LayerMask targetLayer)
        {
            Ray ray = new Ray(from, DirectionalVector(from, to));
            RaycastHit2D hit = Physics2D.Raycast(
                from, DirectionalVector(from, to), range, blockLayer);
            if (hit)
            {
                return false;
            }
            else
            {
                if (!InRange(from, to, range, targetLayer))
                {
                    return false;
                }
                return true;
            }
        }

        public static bool InRange(Vector2 from, Vector2 to, float range, LayerMask targetLayer)
        {
            return Physics2D.Raycast(from, DirectionalVector(from, to), range, targetLayer);
        }
    }
}

