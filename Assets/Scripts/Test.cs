﻿using UnityEngine;
using System.Collections;
using PrototypingAPI;
public class Test : MonoBehaviour {

    float angle = 0;
    public GameObject from;
    public GameObject to;
    public float length = 1;
    public LayerMask blockLayer;
    public LayerMask targetLayer;


	void FixedUpdate ()
    {
        Debug.Log(Prototyping.InLineOfSight(from.transform.position, to.transform.position, 
           length, blockLayer, targetLayer));
    }
}
